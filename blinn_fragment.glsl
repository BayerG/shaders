struct Light
{
    vec3 position;
    vec3 direction;
    float angle;
    vec3 color;
    vec3 specularColor;
    float specularIntensity;
    vec3 coolColor;
    vec3 warmColor;
    float rimIntensity;
};

uniform Light lights[5];

varying vec3 varCoord;
varying vec3 varNormal;
varying vec3 varColor;

void main()
{
    vec3 viewVector = normalize(-varCoord);
    for (int i = 0; i < 5; i++)
    {
        vec3 lightVector = lights[i].angle != 0
            ? normalize(lights[i].position - varCoord)
            : normalize(-lights[i].direction);
        vec3 h = normalize(lightVector + viewVector);
        float n = 16;
        gl_FragColor += vec4(lights[i].specularColor * lights[i].specularIntensity * pow(dot(varNormal, h), n), 1);
    }
    gl_FragColor *= vec4(varColor, 1);
}