struct Light
{
    vec3 position;
    vec3 direction;
    float angle;
    vec3 color;
    vec3 specularColor;
    float specularIntensity;
    vec3 coolColor;
    vec3 warmColor;
    float rimIntensity;
};

uniform Light lights[5];

varying vec3 varCoord;
varying vec3 varNormal;
varying vec3 varColor;

void main()
{
    for (int i = 0; i < 5; i++)
    {
        vec3 lightVector = lights[i].angle != 0
            ? normalize(lights[i].position - varCoord)
            : normalize(-lights[i].direction);
        float diff = 0.2 + max(dot(varNormal, lightVector), 0);
        if (diff < 0.4)
            gl_FragColor += vec4(lights[i].color * 0.3, 1);
        else if (diff < 0.7)
            gl_FragColor += vec4(lights[i].color * 0.6, 1);
        else
            gl_FragColor += vec4(lights[i].color, 1);
    }
    gl_FragColor *= vec4(varColor, 1);
}