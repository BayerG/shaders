from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import numpy

# Shaders.
shaders = ['lambert', 'blinn', 'minnaert', 'toon', 'gooch', 'rim']
programs = []
current_program = 0

# Modes.
modes = ['shaders', 'lights']
current_mode = 'shaders'

# Objects.
terrain_size = 1
lamp_size = 0.2
lamps_count = 4
tree_size = 2
snowman_size = 0.7
car_size = 0.05
car_movement_speed = 0.2
car_rotation_speed = 5
car_matrix = [1, 0, 0, 0,
              0, 1, 0, 0,
              0, 0, 1, 0,
              0, 1.05, 0, 1]

# Camera.
camera_distance = 1
camera_x_rotation = 30
camera_y_rotation = 0
camera_angle_delta = 5
camera_matrix = None

# Lights.
# If angle is 0 then the light is a directed light.
# Else if direction is (0, 0, 0) then the light is a point light.
# Else the light is a projector.
lights = [
    {
        'position': (-0.5, 0.25, -0.5),
        'direction': (0, 0, 0),
        'angle': 180,
        'color': (1, 0, 0),
        'specular_color': (1, 0, 0),
        'specular_intensity': 1,
        'cool_color': (0.1, 0, 0),
        'warm_color': (0.5, 0, 0),
        'rim_intensity': 1
    },
    {
        'position': (-0.166, 0.25, -0.5),
        'direction': (0, 0, 0),
        'angle': 180,
        'color': (0, 1, 0),
        'specular_color': (0, 1, 0),
        'specular_intensity': 1,
        'cool_color': (0, 0.1, 0),
        'warm_color': (0, 0.1, 0),
        'rim_intensity': 1
    },
    {
        'position': (0.166, 0.25, -0.5),
        'direction': (0, 0, 0),
        'angle': 180,
        'color': (0, 0, 1),
        'specular_color': (0, 0, 1),
        'specular_intensity': 1,
        'cool_color': (0, 0, 0.5),
        'warm_color': (0, 0, 0.1),
        'rim_intensity': 1
    },
    {
        'position': (0.5, 0.25, -0.5),
        'direction': (0, 0, 0),
        'angle': 180,
        'color': (1, 1, 0),
        'specular_color': (1, 1, 0),
        'specular_intensity': 1,
        'cool_color': (0.1, 0.1, 0),
        'warm_color': (0.5, 0.1, 0),
        'rim_intensity': 1
    },
    {
        'position': (0, 0, 0),
        'direction': (0, 0, -1),
        'angle': 0,
        'color': (0.5, 0.5, 0.5),
        'specular_color': (0.5, 0.5, 0.5),
        'specular_intensity': 1,
        'cool_color': (0.05, 0.05, 0.25),
        'warm_color': (0.25, 0.05, 0.05),
        'rim_intensity': 1
    },
]
lights_usings = [True] * 5

# Colors.
white = (1, 1, 1)
light_gray = (0.8, 0.8, 0.8)
gray = (0.5, 0.5, 0.5)
black = (0, 0, 0)
red = (1, 0, 0)
yellow = (1, 1, 0)
orange = (1, 0.5, 0)
green = (0, 1, 0)
brown = (0.9, 0.6, 0.3)


def init_shaders():
    global programs

    for shader in shaders:
        vertex_source = open(f'{shader}_vertex.glsl').read()
        vertex = create_shader(GL_VERTEX_SHADER, vertex_source)

        fragment_source = open(f'{shader}_fragment.glsl').read()
        fragment = create_shader(GL_FRAGMENT_SHADER, fragment_source)

        program = glCreateProgram()
        glAttachShader(program, vertex)
        glAttachShader(program, fragment)
        glLinkProgram(program)

        programs.append(program)


def render():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glEnable(GL_DEPTH_TEST)
    glUseProgram(programs[current_program])

    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

    handle_camera()
    set_uniforms()
    draw_terrain()
    draw_lamps()
    glTranslate(-0.2, 0, 0)
    draw_tree()
    glTranslate(0.4, 0, 0)
    draw_snowman()
    glTranslate(-0.2, 0, 0)
    glRotate(-120, 0, 1, 0)
    draw_car()

    glutSwapBuffers()


def reshape(width, height):
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60, width / height, 0.1, 100)


def special(key, x, y):
    global camera_x_rotation, camera_y_rotation

    if key == GLUT_KEY_UP:
        camera_x_rotation += camera_angle_delta
    if key == GLUT_KEY_DOWN:
        camera_x_rotation -= camera_angle_delta
    if key == GLUT_KEY_LEFT:
        camera_y_rotation += camera_angle_delta
    if key == GLUT_KEY_RIGHT:
        camera_y_rotation -= camera_angle_delta


def keyboard(key, x, y):
    global current_mode, current_program, car_matrix

    current_matrix = glGetFloat(GL_MODELVIEW_MATRIX)
    glLoadMatrixf(car_matrix)

    # Moving the car.
    if key == b'w':
        glTranslate(car_movement_speed, 0, 0)
    elif key == b's':
        glTranslate(-car_movement_speed, 0, 0)
    elif key == b'a':
        glTranslate(car_movement_speed, 0, 0)
        glRotate(car_rotation_speed, 0, 1, 0)
    elif key == b'd':
        glTranslate(car_movement_speed, 0, 0)
        glRotate(-car_rotation_speed, 0, 1, 0)

    # Handling the mode.
    elif key == b'm':
        current_mode = modes[(modes.index(current_mode) + 1) % len(modes)]
        glutSetWindowTitle(f'Current mode: {current_mode}')
    else:
        if current_mode == 'shaders':
            current_program = int(key) - 1
            glutSetWindowTitle(f'Current shader: {shaders[current_program]}')
        elif current_mode == 'lights':
            lights_usings[int(key) - 1] = not lights_usings[int(key) - 1]
            glutSetWindowTitle(f'Light {int(key)} is {"on" if lights_usings[int(key) - 1] else "off"}')
    car_matrix = glGetFloat(GL_MODELVIEW_MATRIX)
    glLoadMatrixf(current_matrix)


def create_shader(shader_type, source):
    shader = glCreateShader(shader_type)
    glShaderSource(shader, source)
    glCompileShader(shader)
    return shader


def handle_camera():
    global camera_matrix

    glTranslate(0, 0, -camera_distance)
    glRotate(camera_x_rotation, 1, 0, 0)
    glRotate(camera_y_rotation, 0, 1, 0)
    camera_matrix = numpy.array(glGetFloat(GL_MODELVIEW_MATRIX)).reshape(4, 4)


def set_uniforms():
    for i in range(len(lights)):
        local_light_position = numpy.dot(lights[i]['position'] + (1,), camera_matrix)
        local_light_direction = numpy.dot(lights[i]['direction'] + (1,), numpy.transpose(numpy.linalg.inv(camera_matrix)))

        if lights_usings[i]:
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].position'),
                        local_light_position[0], local_light_position[1], local_light_position[2])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].direction'),
                        local_light_direction[0], local_light_direction[1], local_light_direction[2])
            glUniform1f(glGetUniformLocation(programs[current_program], f'lights[{i}].angle'),
                        lights[i]['angle'])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].color'),
                        lights[i]['color'][0], lights[i]['color'][1], lights[i]['color'][2])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].specularColor'),
                        lights[i]['specular_color'][0], lights[i]['specular_color'][1], lights[i]['specular_color'][2])
            glUniform1f(glGetUniformLocation(programs[current_program], f'lights[{i}].specularIntensity'),
                        lights[i]['specular_intensity'])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].coolColor'),
                        lights[i]['cool_color'][0], lights[i]['cool_color'][1], lights[i]['cool_color'][2])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].warmColor'),
                        lights[i]['warm_color'][0], lights[i]['warm_color'][1], lights[i]['warm_color'][2])
            glUniform1f(glGetUniformLocation(programs[current_program], f'lights[{i}].rimIntensity'),
                        lights[i]['rim_intensity'])
        else:
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].position'),
                        local_light_position[0], local_light_position[1], local_light_position[2])
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].color'),
                        0, 0, 0)
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].specularColor'),
                        0, 0, 0)
            glUniform1f(glGetUniformLocation(programs[current_program], f'lights[{i}].specularIntensity'),
                        0)
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].coolColor'),
                        0, 0, 0)
            glUniform3f(glGetUniformLocation(programs[current_program], f'lights[{i}].warmColor'),
                        0, 0, 0)
            glUniform1f(glGetUniformLocation(programs[current_program], f'lights[{i}].rimIntensity'),
                        0)


def draw_terrain():
    glColor(light_gray)
    glBegin(GL_QUADS)
    glNormal(0, 1, 0)
    glVertex(-terrain_size / 2, 0, -terrain_size / 2)
    glNormal(0, 1, 0)
    glVertex(terrain_size / 2, 0, -terrain_size / 2)
    glNormal(0, 1, 0)
    glVertex(terrain_size / 2, 0, terrain_size / 2)
    glNormal(0, 1, 0)
    glVertex(-terrain_size / 2, 0, terrain_size / 2)
    glEnd()


def draw_lamps():
    for lamp_position in [(x, -terrain_size / 2) for x in
                          numpy.linspace(-terrain_size / 2, terrain_size / 2, lamps_count)]:
        x, z = lamp_position
        glPushMatrix()
        glTranslate(x, 0, z)
        glRotate(-90, 1, 0, 0)
        glColor(gray)
        glutSolidCylinder(lamp_size / 20, lamp_size, 10, 10)
        glTranslate(0, 0, lamp_size + lamp_size / 4)
        glColor(yellow)
        glutWireSphere(lamp_size / 4, 10, 10)
        glPopMatrix()


def draw_tree():
    glPushMatrix()

    glScale(tree_size, tree_size, tree_size)
    glScale(0.2, 0.2, 0.2)
    glRotate(-90, 1, 0, 0)
    glColor(brown)
    glutSolidCylinder(0.1, 0.3, 10, 10)

    glTranslate(0, 0, 0.3)
    glColor(green)
    glutSolidCone(0.4, 0.3, 10, 10)

    glTranslate(0, 0, 0.3)
    glScale(0.7, 0.7, 0.7)
    glutSolidCone(0.4, 0.3, 10, 10)

    glTranslate(0, 0, 0.3)
    glScale(0.7, 0.7, 0.7)
    glutSolidCone(0.4, 0.3, 10, 10)

    glPopMatrix()


def draw_snowman():
    glPushMatrix()

    glScale(snowman_size, snowman_size, snowman_size)
    glTranslate(0, 0.1, 0)
    glColor(white)

    for _ in range(3):
        glutSolidSphere(0.1, 10, 10)
        glTranslate(0, 0.15, 0)
        glScale(0.8, 0.8, 0.8)

    glPopMatrix()


def draw_car():
    glPushMatrix()

    glScale(car_size, car_size, car_size)
    glMultMatrixf(car_matrix)

    glPushMatrix()
    glTranslate(-1, 0, 0)
    glScale(3, 1.5, 1)
    glColor(red)
    glutSolidCube(1)
    glPopMatrix()

    glPushMatrix()
    glTranslate(1, -0.25, 0)
    glColor(orange)
    glutSolidCube(1)
    glPopMatrix()

    glPushMatrix()
    glTranslate(1, -0.75, 0.5)
    glColor(black)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(0, 0, -1)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(-2.5, 0, 0)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glTranslate(0, 0, 1)
    glutSolidTorus(0.1, 0.2, 10, 10)
    glPopMatrix()

    glPushMatrix()
    glTranslate(1.5, -0.5, 0.3)
    glColor(yellow)
    glutWireSphere(0.1, 10, 10)
    glTranslate(0, 0, -0.6)
    glutWireSphere(0.1, 10, 10)
    glPopMatrix()

    glPopMatrix()


def main():
    glutInit()
    glutInitWindowSize(600, 600)
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH)
    glutCreateWindow(f'Current shader: {shaders[current_program]}')
    glutIdleFunc(render)
    glutDisplayFunc(render)
    glutReshapeFunc(reshape)
    glutSpecialFunc(special)
    glutKeyboardFunc(keyboard)

    glClearColor(0, 0.6, 1, 1)
    glEnable(GL_DEPTH_TEST)
    init_shaders()

    glutMainLoop()


if __name__ == '__main__':
    main()
