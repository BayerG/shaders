struct Light
{
    vec3 position;
    vec3 direction;
    float angle;
    vec3 color;
    vec3 specularColor;
    float specularIntensity;
    vec3 coolColor;
    vec3 warmColor;
    float rimIntensity;
};

uniform Light lights[5];

varying vec3 varCoord;
varying vec3 varNormal;
varying vec3 varColor;

void main()
{
    vec3 viewVector = normalize(-varCoord);
    for (int i = 0; i < 5; i++)
    {
        vec3 lightVector = lights[i].angle != 0
            ? normalize(lights[i].position - varCoord)
            : normalize(-lights[i].direction);
        vec3 reflectVector = normalize(reflect(-lightVector, varNormal));
        vec3 kCool = min(lights[i].coolColor + lights[i].coolColor * varColor, 1);
        vec3 kWarm = min(lights[i].warmColor + lights[i].warmColor * varColor, 1);
        vec3 kFinal = mix(kCool, kWarm, dot(varNormal, lightVector));
        float spec = pow(max(dot(reflectVector, viewVector), 0), 32);
        gl_FragColor += vec4(min(kFinal + spec, 1), 1);
    }
}